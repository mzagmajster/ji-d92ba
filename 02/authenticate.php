<?php

// Config (correct values if you want to test it).
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', 'podlasica');
define('DB_NAME', 'ji_d92ba');

// Just for testing.
function getUser($id) {
	$rvalue = NULL;
	$fid = filter_var($id, FILTER_VALIDATE_INT);

	if($fid === false) {
		return NULL;
	}

	$con = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

	if($con->connect_errno) {
		return NULL;
	}
	
	$sql = "SELECT * FROM user WHERE id=$id; ";
	$res = $con->query($sql);

	if($res) {
		$rvalue = $res->fetch_assoc();
		$res->free();
	}
	$con->close();

	return $rvalue;
}


function authenticate() {
	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}

	$user = NULL;
	if(isset($_SESSION['__CRM__'])) {
		$user = getUser($_SESSION['__CRM__']['user']['id']);
	}

	if($user === NULL) {
		echo "Verification of CRM user failed.";
		return;
	}

	// Load front-end user.
	$fuser = getUser($_GET['id']);
	if($fuser === NULL) {
		echo "Front-end user with ID: " . $_GET['id'] . "not found.<br/>";
		return;
	}

	$_SESSION['_WWW_'] = array('user' => $fuser);

	echo "Authentication successful!<br/>";
}

// Just for testing.
function addCrmUser($id) {
	$u = getUser($id);
	if(!$u) {
		echo "CRM user not found.<br/>";
		return;
	}
	else if($u['account_type'] != 'staff') {
		echo "User with ID: $id is not CRM.";
		return;
	}

	if(session_status() == PHP_SESSION_NONE) {
		session_start();
	}

	$_SESSION['__CRM__'] = array('user' => $u);
}

if(isset($_GET['crm'])) {
	addCrmUser($_GET['crm']);
}
else if(isset($_GET['id'])) {
	authenticate();
}
else {
	die('Invalid params. Please specify URL with id or crm var.');
}

