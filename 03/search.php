<?php
function search($term)
{
	/* MySQL search function returns data in this structure:
	$results = array(
	array(
	'title' => 'Idea #1'
	'description' => 'Lorem ipsum dolor sit amet non est ...',
	),
	array(
	'title' => 'Idea #2'
	'description' => 'Lorem ipsum dolor sit amet non est ...',
	),
	);
	If there are no results the above array is empty.
	*/
	$archive = array(
		array(
			'title' => 'Idea #1',
			'description' => 'Lorem ipsum dolor sit amet non est ...'
		),
		array(
			'title' => 'Idea #2',
			'description' => 'Idea 2 description'
		),
		array(
			'title' => 'Idea #3',
			'description' => 'Idea 3 description',
		),
		array(
			'title' => 'Idea #4',
			'description' => 'Idea 4 description'
		),
		array(
			'title' => 'Idea #5',
			'description' => 'Idea 5 description'
		)
	);
	$results = array();
	foreach ($archive as $record) {
		$c = false;
		if(strpos($record['title'], $term) !== false) {
			$c = true;
		}
		else if(strpos($record['description'], $term) !== false) {
			$c = true;
		}

		if($c) {
			$results[] = $record;
		}
	}

	return json_encode($results);
}

if(!isset($_POST['term'])) {
	echo json_encode(array());
}
else {
	echo search($_POST['term']);
}
