# Inserts.
INSERT INTO idea (title, description, inventor_id) VALUES('Avtomobilski rocaj za prtljaznik', 'Prirocen rocaj za lazje odpiranje vrat.', 3);
INSERT INTO idea (title, description, inventor_id) VALUES('Prilagodljiva avtomobilska guma', 'Prirocen mehanizem razvit v lanskem letu nam je omogocil razvoj revolucionarne avtomobilske gume.', 3);
INSERT INTO idea (title, description, inventor_id) VALUES('Izboljsava avto pilota', 'Avto pilot, ki bo izboljsal in olajsal letenje vsakega letala.', 3);
INSERT INTO idea (title, description, inventor_id) VALUES('Prenovljen nevigacijski sistem', 'Navigacija, ki predstavlja revolucijo na trgu navigacijskih naprav.', 3);
INSERT INTO idea (title, description, inventor_id) VALUES('Kontrolor naprav', 'Mini visoko inteligentni robot nam omogoca nadzor nad vec gospodinjskimi napravami.', 3);
INSERT INTO idea (title, description, inventor_id) VALUES('Siri Generacije Zeta', 'Siri se bo od sedaj naprej z vami se lazje in bolj tekoce pogovarjala.', 3);

# ALTER
ALTER TABLE idea ADD FULLTEXT(title, description);

# SELECT
SELECT * FROM idea WHERE MATCH(title, description)
AGAINST ('string' IN BOOLEAN MODE);


