CREATE TABLE idea (
	id INT NOT NULL AUTO_INCREMENT,
	title VARCHAR(45) NULL,
	description TEXT NULL,
	created TIMESTAMP NULL,
	inventor_id INT NOT NULL,
	PRIMARY KEY (id),
	INDEX fk_idea_user_idx (inventor_id ASC),
	CONSTRAINT fk_idea_user
	FOREIGN KEY (inventor_id)
	REFERENCES user (id)
	ON DELETE CASCADE
	ON UPDATE CASCADE
)engine = innodb;
