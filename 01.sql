CREATE TABLE user (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(80) NOT NULL,
	email VARCHAR(80) NOT NULL,
	password TEXT NOT NULL,
	first_name CHAR(80) DEFAULT NULL,
	last_name CHAR(80) DEFAULT NULL,
	created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	account_type ENUM('staff', 'inventor', 'distributor') DEFAULT 'inventor'
) engine=innodb;
